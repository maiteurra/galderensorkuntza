import torch
from torch.utils.data.dataset import Dataset
from torch.utils.data.sampler import Sampler
from torch._six import int_classes as _int_classes
import numpy as np

from utils import constants


class DatasetTrain(Dataset):

    def __init__(self, data):
        self.src = data['src']
        self.tgt = data['tgt']

    def __len__(self):
        return len(self.src)

    def __getitem__(self, idx):
        return self.src[idx], self.tgt[idx]


class DatasetGenerate(Dataset):

    def __init__(self, data, src_dict, file=True):
        self.src = []
        if file:
            sf = open(data, 'r', encoding='utf-8').read()
        else:
            sf = data
        srcWords = [line.split() for line in sf.splitlines()]
        for i in range(len(srcWords)):
            self.src.append(src_dict.convertToIdx(srcWords[i], unkWord=constants.UNK_WORD))

    def __len__(self):
        return len(self.src)

    def __getitem__(self, idx):
        return self.src[idx]


def pad_tensor(vec, pad, dim, left=False):
    if left:
        pad_size = list(vec.shape)
        pad_size[dim] = pad - vec.size(dim)
        return torch.cat([(torch.from_numpy(np.full(*pad_size, constants.PAD))), vec], dim=dim)

    else:
        pad_size = list(vec.shape)
        pad_size[dim] = pad - vec.size(dim)
        return torch.cat([vec, (torch.from_numpy(np.full(*pad_size, constants.PAD)))], dim=dim)


class Padder:

    def __init__(self, dim, tgt=True):
        '''
			dim - the dimension to be padded
		'''
        self.dim = dim
        self.tgt = tgt

    def pad(self, batch):

        if self.tgt:

            len_x = [x[0].shape[self.dim] for x in batch]
            max_len_x = max(len_x)
            len_y = [x[1].shape[self.dim] for x in batch]
            max_len_y = max(len_y)

            pad_batch = [
                [pad_tensor(x, pad=max_len_x, dim=self.dim, left=True), pad_tensor(y, pad=max_len_y, dim=self.dim)] for
                x, y in batch]

            xs = torch.stack([x[0] for x in pad_batch], dim=0)
            ys = torch.stack([x[1] for x in pad_batch], dim=0)

            return xs, ys, len_x, len_y

        else:

            len_x = [x.shape[self.dim] for x in batch]
            max_len_x = max(len_x)
            xs = torch.stack([pad_tensor(x, pad=max_len_x, dim=self.dim, left=True) for x in batch], dim=0)
            return xs, len_x

    def __call__(self, batch):
        return self.pad(batch)


class BatchSamplerRandom(Sampler):

    def __init__(self, sampler, batch_size, drop_last):
        if not isinstance(sampler, Sampler):
            raise ValueError("sampler should be an instance of "
                             "torch.utils.data.Sampler, but got sampler={}"
                             .format(sampler))
        if not isinstance(batch_size, _int_classes) or isinstance(batch_size, bool) or batch_size <= 0:
            raise ValueError("batch_size should be a positive integeral value, "
                             "but got batch_size={}".format(batch_size))
        if not isinstance(drop_last, bool):
            raise ValueError("drop_last should be a boolean value, but got "
                             "drop_last={}".format(drop_last))
        self.sampler = sampler
        self.batch_size = batch_size
        self.drop_last = drop_last

    def __iter__(self):
        batches = []
        batch = []
        for idx in self.sampler:
            batch.append(idx)
            if len(batch) == self.batch_size:
                batch.reverse()
                batches.append(batch)
                batch = []
            if len(batch) > 0 and not self.drop_last:
                batches.append(batch.reverse())
        batch_order = torch.randperm(len(batches))
        for idx in batch_order:
            yield batches[idx]

    def __len__(self):
        if self.drop_last:
            return len(self.sampler) // self.batch_size
        else:
            return (len(self.sampler) + self.batch_size - 1) // self.batch_size
