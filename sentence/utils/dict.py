import torch


class Dict(object):
	def __init__(self, data=None):
		self.idxToWord = {}
		self.wordToIdx = {}
		self.frequencies = {}

		#Special entries will not be pruned
		self.special = []

		if data != None:
			if type(data) == 'str':
				self.loadFile(data)
			else:
				self.addSpecials(data)

	def size(self):
		return len(self.idxToWord)

	def loadFile(self, filename):
		with open(filename, 'r', encoding='utf-8') as f:
			for line in f:
				word_idx = line.split()
				word = word_idx[0]
				idx = int(word_idx[-1])

				self.add(word,idx)


	def writeFile(self, filename):
		with open(filename, 'w', encoding='utf-8') as f:
			for i in self.idxToWord.keys():
				word = self.idxToWord[i]
				f.write(word +' ' + str(i) + '\n')

	#Lookup 'key' in the dictionary: it can be an index or a string
	def lookup(self, key):
		if isinstance(key,str):
			if key in self.wordToIdx:
				return self.wordToIdx[key]
			else:
				return None
		else:
			return self.idxToWord[key]

	def addSpecial(self, word, idx=None):
		idx = self.add(word, idx, True)
		self.special.append(idx)

	def addSpecials(self, words):
		for word in words:
			self.addSpecial(word)

	#Add 'word' to dictionary, return its index
	def add(self, word, idx=None, s=False):
		if idx != None:
			self.idxToWord[idx] = word
			self.wordToIdx[word] = idx
		else:
			if word not in self.wordToIdx:
				idx = len(self.idxToWord)
				self.idxToWord[idx] = word
				self.wordToIdx[word] = idx
			else:
				idx = self.wordToIdx[word]
			
		if s == False:
			if idx in self.frequencies:
				self.frequencies[idx] += 1
			else:
				self.frequencies[idx] = 1

		return idx

	def prune(self, size):
		if size >= self.size():
			return self

		freq = [[k,v] for k,v in self.frequencies.items()]
		freq = sorted(freq, key=lambda x: x[1], reverse=True)

		newDict = Dict()
		newDict.addSpecials([self.idxToWord[i] for i in self.special])

		for i in range(size):
			newDict.add(self.idxToWord[freq[i][0]])

		return newDict


	#Convert 'words' to indices. Use 'unkWord' if not found
	def convertToIdx(self, words, unkWord=None, bosWord=None, eosWord=None):
		vec = []

		if bosWord != None:
			vec.append(self.lookup(bosWord))

		for word in words:
			idx = self.lookup(word)
			if idx == None:
				idx = self.lookup(unkWord)
			vec.append(idx)

		if eosWord != None:
			vec.append(self.lookup(eosWord))

		return torch.tensor(vec)


	def convertToWords(self, idx, stop=None):
		words = []

		for i in idx:
			words.append(self.lookup(i))
			if i == stop:
				break

		return words