

def cpu(x):
    return x.cpu() if x is not None else None


def gpu(x):
    return x.cuda() if x is not None else None


default = cpu