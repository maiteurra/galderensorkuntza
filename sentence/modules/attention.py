import torch
import torch.nn as nn


class GlobalAttention(nn.Module):
	def __init__(self, dim, mode):
		super(GlobalAttention, self).__init__()

		self.mode = mode

		self.linear_alig = nn.Linear(dim,dim, bias=False)
		self.softmax = nn.Softmax(dim=2)

		self.linear = nn.Linear(2*dim, dim, bias=False)
		self.linear_none = nn.Linear(dim, dim, bias=False)
		self.tanh = nn.Tanh()

	def forward(self, source, target, mask=None):

		if self.mode == 'none':
			output = self.tanh(self.linear_none(target.squeeze(1)))
			attention = torch.zeros(())
		else: 
			# Get attention
			if self.mode == 'dot':
				align = torch.bmm(target,source.transpose(1,2))
			elif self.mode == 'general':
				q = self.linear_alig(source).transpose(1,2)
				align = target.bmm(q)
			else:
				print('Not valid mode. Options are: none, dot, general.')

			if mask is not None:
					align.data.masked_fill_(mask.unsqueeze(1), -float('inf'))

			attention = self.softmax(align)

			# Apply attention to context
			c_t = torch.bmm(attention,source).squeeze(1)
			output = self.tanh(self.linear(torch.cat((target.squeeze(1), c_t), dim=1)))

		return output, attention
