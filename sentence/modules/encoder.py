import torch
import torch.nn as nn


class Encoder(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_size, num_layers, dropout):
        super(Encoder, self).__init__()
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.hidden_size = hidden_size
        self.num_layers = num_layers

        self.lstm = nn.LSTM(input_size=embedding_dim, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,
                            dropout=dropout, bidirectional=True)

    def forward(self, inputs, inputs_lengths, embeddings):
        embedded = embeddings(inputs)
        embedded = torch.nn.utils.rnn.pack_padded_sequence(embedded, inputs_lengths, batch_first=True)
        outputs, hidden = self.lstm(embedded)
        outputs, output_lengths = torch.nn.utils.rnn.pad_packed_sequence(outputs, batch_first=True)
        return outputs, output_lengths, hidden
