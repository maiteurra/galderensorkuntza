import torch
import torch.nn as nn

from .attention import GlobalAttention


class Decoder(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_size, num_layers, dropout, attn_mode):
        super(Decoder, self).__init__()

        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.hidden_size = hidden_size
        self.num_layers = num_layers

        self.dropout = nn.Dropout(dropout)
        self.attn = GlobalAttention(hidden_size, attn_mode)
        self.lstm = nn.LSTM(input_size=embedding_dim, hidden_size=hidden_size, num_layers=num_layers, batch_first=True,
                            dropout=dropout)
        self.out = nn.Linear(hidden_size, vocab_size, bias=False)
        self.logsoftmax = nn.LogSoftmax(dim=1)

    def forward_step(self, inputs, hidden, encoder_outputs, encoder_mask, embeddings):
        embedded = embeddings(inputs)
        output, hidden = self.lstm(embedded, hidden)

        output, attn = self.attn(encoder_outputs, output, encoder_mask)

        predicted_logsoftmax = self.logsoftmax(self.out(output))

        return predicted_logsoftmax, hidden, attn

    def forward(self, max_len, batch_size, inputs=None, decoder_hidden=None, encoder_outputs=None, encoder_mask=None,
                train=False, embeddings_dec=None):

        decoder_outputs = []
        symbols = inputs[:, 0].unsqueeze(1)

        for di in range(max_len):
            if train:
                decoder_input = inputs[:, di].unsqueeze(1)
            else:
                decoder_input = inputs[:, di].unsqueeze(1)  # symbols

            decoder_output, decoder_hidden, step_attn = self.forward_step(decoder_input, decoder_hidden,
                                                                          encoder_outputs, encoder_mask, embeddings_dec)
            decoder_outputs.append(decoder_output)
            symbols = decoder_outputs[-1].topk(1)[1]

        return torch.stack(decoder_outputs, dim=2), decoder_hidden

    def init_state(self, encoder_hidden):
        encoder_hidden = tuple([self._cat_directions(h) for h in encoder_hidden])
        return encoder_hidden

    def _cat_directions(self, h):

        h = h.view(int(h.size(0) / 2), 2, h.size(1), h.size(2))
        h = torch.cat([h[:, 0, :, :], h[:, 1, :, :]], 2)

        return h
