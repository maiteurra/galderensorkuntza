import torch
import torch.nn as nn

import utils.constants as constants
import utils.device as device

to_device = device.gpu if torch.cuda.is_available else device.cpu


class Generator(nn.Module):
    def __init__(self, encoder=None, decoder=None, criterion=None, src_dictionary=None, tgt_dictionary=None):
        super(Generator, self).__init__()

        self.encoder = encoder
        self.decoder = decoder
        self.criterion = criterion

        self.src_dictionary = src_dictionary
        self.tgt_dictionary = tgt_dictionary

    def _train(self, mode):
        self.encoder.train(mode)
        self.decoder.train(mode)
        self.criterion.train(mode)

    def mask(self, lengths, max_length):
        batch_size = len(lengths)
        mask = torch.ByteTensor(batch_size, max_length).fill_(0)

        for i in range(batch_size):
            for j in range(0, max_length - lengths[i]):
                mask[i, j] = 1

        return to_device(mask) if torch.cuda.is_available() else mask

    def score(self, src, tgt, max_len, batch_size, lengths, embeddings_enc, embeddings_dec, train):
        self._train(train)

        # Encode
        context, context_lengths, encoder_hidden = self.encoder(src, lengths, embeddings_enc)
        context_mask = self.mask(context_lengths, src.size(1))

        # Decode
        decoder_hidden = self.decoder.init_state(encoder_hidden)
        decoder_outputs, decoder_hidden = self.decoder(max_len, batch_size, tgt[:, 0:max_len], decoder_hidden, context,
                                                       context_mask, train, embeddings_dec)

        # Compute loss
        loss = 0
        for i in range(max_len):
            loss += self.criterion(decoder_outputs[:, :, i], tgt[:, i + 1])

        return loss

    def beam_search(self, src, lengths, embeddings_enc, embeddings_dec, beam_size, max_len):
        self._train(False)

        batch_size = src.size(0)

        # Encode
        context, context_lengths, encoder_hidden = self.encoder(src, lengths, embeddings_enc)

        # Initialize
        decoder_hidden = self.decoder.init_state(encoder_hidden)
        sequences = [[] for _ in range(batch_size)]
        sequence_scores = batch_size * [-float('inf')]
        sequence_attns = batch_size * [[]]
        sequence_hypotheses = batch_size * [(0.0, [], [])] + (beam_size - 1) * batch_size * [
            (-float('inf'), [], [])]  # (score, sequence, attn)
        prev_words = beam_size * batch_size * [constants.BOS]
        not_finished = set(range(batch_size))

        # Inflate
        decoder_hidden = tuple([h.repeat(1, beam_size, 1) for h in decoder_hidden])
        context = context.repeat(beam_size, 1, 1)
        context_lengths = context_lengths.repeat(beam_size)
        context_mask = self.mask(context_lengths, src.size(1))

        while not_finished:

            prev = to_device(torch.LongTensor([prev_words])).transpose(0,
                                                                       1) if torch.cuda.is_available() else torch.LongTensor(
                [prev_words]).transpose(0, 1)
            logprobs, decoder_hidden, step_attn = self.decoder.forward_step(prev, decoder_hidden, context, context_mask,
                                                                            embeddings_dec)
            prev_words = logprobs.max(dim=1)[1].data.cpu().numpy().tolist()

            word_scores, words = logprobs.topk(k=beam_size, dim=1, sorted=False)
            word_scores = word_scores.data.cpu().numpy().tolist()
            words = words.data.cpu().numpy().tolist()

            for i in not_finished.copy():
                candidates = []  # (score, index, word, attn)
                for j in range(beam_size):
                    idx = j * batch_size + i

                    for k in range(beam_size):
                        word = words[idx][k]
                        score = sequence_hypotheses[idx][0] + word_scores[idx][k]
                        attn = sequence_hypotheses[idx][2] + [step_attn[idx, :, :]]

                        if word != constants.EOS:
                            candidates.append((score, idx, word, attn))
                        elif score > sequence_scores[i]:
                            sequences[i] = sequence_hypotheses[idx][1] + [word]
                            sequence_scores[i] = score
                            sequence_attns[i] = attn

                top = []  # (score, word, translation, h1, h2, attn)
                for score, idx, word, attn in sorted(candidates, reverse=True)[:beam_size]:
                    sequence = sequence_hypotheses[idx][1] + [word]
                    top.append(
                        (score, word, sequence, decoder_hidden[0][:, idx, :], decoder_hidden[1][:, idx, :], attn))

                for j, (score, word, sequence, h1, h2, attn) in enumerate(top):
                    next_idx = j * batch_size + i
                    sequence_hypotheses[next_idx] = (score, sequence, attn)
                    prev_words[next_idx] = word
                    decoder_hidden[0][:, next_idx, :] = h1
                    decoder_hidden[1][:, next_idx, :] = h2

                if len(sequence_hypotheses[i][1]) >= max_len or sequence_scores[i] > sequence_hypotheses[i][0]:
                    not_finished.discard(i)
                    if len(sequences[i]) == 0:
                        sequences[i] = sequence_hypotheses[i][1]
                        sequence_scores[i] = sequence_hypotheses[i][0]
                        sequence_attns[i] = sequence_hypotheses[i][2]

        return sequences, sequence_scores, sequence_attns
