import numpy as np
import torch

array = np.load('../data/qg.src.840B.300d.npy')
array = torch.from_numpy(array)
torch.save(array,"../data/embs/qg.src.840B.300d.pth")

array = np.load('../data/qg.tgt.840B.300d.npy')
array = torch.from_numpy(array)
torch.save(array,"../data/embs/qg.tgt.840B.300d.pth")
