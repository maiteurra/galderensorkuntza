import sys

sys.path.append("..")

import warnings

warnings.filterwarnings("ignore")

from flask import Flask, jsonify, request, render_template
import torch
import torch.nn as nn
import nltk
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler

from utils import DatasetGenerate, Padder, device, constants

HOST = '0.0.0.0'
PORT = 8888

app = Flask(__name__)

to_device = device.gpu if torch.cuda.is_available else device.cpu

# Load model
model = torch.load('840B.300d.600rnn.6.24.2392.pth', map_location=torch.device('cpu'))

embeddings_encoder = torch.load('../data/embs/qg.src.840B.300d.pth', map_location=torch.device('cpu'))
embeddings_decoder = torch.load('../data/embs/qg.tgt.840B.300d.pth', map_location=torch.device('cpu'))
embeddings_enc = nn.Embedding.from_pretrained(embeddings_encoder, freeze=True)
embeddings_dec = nn.Embedding.from_pretrained(embeddings_decoder, freeze=True)

beam_size = 3
max_sent_length = 50


@app.route('/')
def home():
    return render_template("home.html")


def _replace_unk(seq_words, src_words, sequence_attns):
    for i, word in enumerate(seq_words):
        if word == constants.UNK_WORD:
            idx = torch.argmax(sequence_attns[i])
            seq_words[i] = src_words[idx]

    return seq_words


def _preprocess_text(text):
    text = text.replace(')', ' -rrb-').replace('(', '-lrb- ')
    out = ""
    for paragraph in text.split('\n'):
        for sentence in nltk.sent_tokenize(paragraph):
            out += " ".join(nltk.word_tokenize(sentence)) + '\n'
    return out


def _postprocess_text(text):
    text = text.replace(' .', '.').replace(' ,', ',').replace(' :', ':').replace(' ;', ';').replace(' !', '!').replace(
        ' ?', '?').replace(" '", "'").replace(' -rrb-', ')').replace('-lrb- ', '(')
    return text


def get_prediction(text):
    results = {'question': {}, 'score': {}}
    # Load source
    text = _preprocess_text(text)

    dataset = DatasetGenerate(text, model.src_dictionary, file=False)

    loader = DataLoader(dataset, 1, sampler=SequentialSampler(dataset), drop_last=False,
                        collate_fn=Padder(dim=0, tgt=False))

    src_words = [line.split() for line in text.splitlines()]

    questions, scores = [], []
    # Generate questions

    for idx, (src, lengths) in enumerate(loader):
        src_words_seq = src_words[idx]

        sequences, sequence_scores, sequence_attns = model.beam_search(src, lengths, embeddings_enc, embeddings_dec,
                                                                       beam_size, max_sent_length)
        question_words = model.tgt_dictionary.convertToWords(sequences[0][:-1])
        question_words = _replace_unk(question_words, src_words_seq, sequence_attns[0][:])

        questions.append((" ".join(question_words)).capitalize())
        scores.append(sequence_scores[0])

    # return questions, scores
    questions = "\n".join(questions)
    questions = _postprocess_text(questions)
    return questions, scores


@app.route('/predict', methods=['POST'])
def predict():
    if request.method == 'POST':
        text = request.form['text']
        question, scores = get_prediction(text)
    return jsonify({'question': question, 'score': scores})


if __name__ == '__main__':
    app.run()
