import torch
from torch import optim
import torch.nn as nn
from torch.nn.utils import clip_grad_norm
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
import argparse
import numpy as np

from modules import Encoder, Decoder, Generator
from utils import DatasetTrain, BatchSamplerRandom, Padder, constants, device

parser = argparse.ArgumentParser(description='train')

parser.add_argument('--data', type=str, default='', required=True,
                    help='path to the training *-train.pth file from preprocess.py')
parser.add_argument('--save_model', type=str, default='', required=True,
                    help='Model filename (the model will be saved as <save_model>_epochN_PPL.t7 where PPL is the validation perplexity')

## Model options
parser.add_argument('--layers', type=int, default=2, help='Number of layers in the LSTM encoder/decoder')
parser.add_argument('--rnn_size', type=int, default=500, help='Size of LSTM hidden states')
parser.add_argument('--word_vec_size', type=int, default=500, help='Word embedding sizes')
parser.add_argument('--attn_mode', type=str, default='general', help='Attention mode, options: none, dot, general')

## Optimitation options
parser.add_argument('--max_batch_size', type=int, default=64, help='Maximum batch size')
parser.add_argument('--epochs', type=int, default=13, help='Number of training epoch')
parser.add_argument('--param_init', type=float, default=0.1,
                    help='Parameters are initialized over uniform distribution with support (-param_init, param_init)')
parser.add_argument('--learning_rate', type=float, default=1,
                    help='Starting learning rate. If adagrad/adadelta/adam is used, then this is the global learning rate. Recommended settings are: sgd = 1, adagrad = 0.1, adadelta = 1, adam = 0.0002')
parser.add_argument('--max_grad_norm', type=float, default=5,
                    help='If the norm of the gradient vector exceeds this renormalize it to have the norm equal to max_grad_norm')
parser.add_argument('--dropout', type=float, default=0.3,
                    help='Dropout probability. Dropout is applied between vertical LSTM stacks.')
parser.add_argument('--learning_rate_decay', type=float, default=0.5,
                    help='Decay learning rate by this much if (i) perplexity does not decrease on the validation set or (ii) epoch has gone past the start_decay_at_limit')
parser.add_argument('--start_decay_at', type=float, default=0.5, help='Start decay after this epoch')
parser.add_argument('--pre_word_vecs_enc', type=str, default='',
                    help='If a valid path is specified, then this will load pretrained word embeddings on the encoder side. See README for specific formatting instructions.')
parser.add_argument('--pre_word_vecs_dec', type=str, default='',
                    help='If a valid path is specified, then this will load pretrained word embeddings on the decoder side. See README for specific formatting instructions.')
parser.add_argument('--fix_word_vecs_enc', type=bool, default=False, help='Fix word embeddings on the encoder side')
parser.add_argument('--fix_word_vecs_dec', type=bool, default=False, help='Fix word embeddings on the decoder side')

# Other options
parser.add_argument('--save_every', type=int, default=0,
                    help='Save intermediate models every this many iterations within an epoch. If = 0, will not save models within an epoch.')
parser.add_argument('--report_every', type=int, default=50,
                    help='Print stats every this many iterations within an epoch.')
parser.add_argument('--seed', type=int, default=6439, help='Seed for random initialization')

# Parse arguments
args = parser.parse_args()

torch.manual_seed(args.seed)

to_device = device.gpu if torch.cuda.is_available else device.cpu


def updateLearningRate(optimizer, epoch):
    if epoch >= args.start_decay_at:
        for param_group in optimizer.param_groups:
            param_group['lr'] *= args.learning_rate_decay


def saveModel(model, filename):
    torch.save(model, filename)


def validateEpoch(model, dataset_valid, embeddings_enc, embeddings_dec):
    # Data loader
    valid_loader = DataLoader(dataset_valid,
                              batch_sampler=BatchSamplerRandom(SequentialSampler(dataset_valid), args.max_batch_size,
                                                               drop_last=True), collate_fn=Padder(dim=0))

    total_loss = 0
    tgt_word_count = 0
    for batch_idx, (src, tgt, len_x, len_y) in enumerate(valid_loader):
        src = to_device(src)
        tgt = to_device(tgt)

        tgt_word_count += sum(len_y)

        batch_size = src.size(0)
        max_len_y = tgt.size(1)

        # Compute loss
        loss = model.score(src, tgt, max_len_y - 1, batch_size, len_x, embeddings_enc, embeddings_dec, train=False)
        total_loss += loss.item()

    print('Validation perplexity:' + str(np.exp(total_loss / tgt_word_count)))
    print('')

    return np.exp(total_loss / tgt_word_count)


def trainEpoch(model, dataset_train, optimizer, epoch_n, embeddings_enc, embeddings_dec):
    # Data loader
    train_loader = DataLoader(dataset_train,
                              batch_sampler=BatchSamplerRandom(SequentialSampler(dataset_train), args.max_batch_size,
                                                               drop_last=True), collate_fn=Padder(dim=0))

    n_iterations = len(train_loader)
    lr = optimizer.param_groups[0]['lr']

    total_loss = 0
    src_word_count = 0
    tgt_word_count = 0

    for batch_idx, (src, tgt, len_x, len_y) in enumerate(train_loader):

        src = to_device(src)
        tgt = to_device(tgt)

        # Reset gradients
        optimizer.zero_grad()

        # Words count
        tgt_word_count += sum(len_y)

        batch_size = src.size(0)
        max_len_y = tgt.size(1)

        # Compute loss
        loss = model.score(src, tgt, max_len_y - 1, batch_size, len_x, embeddings_enc, embeddings_dec, train=True)
        total_loss += loss.item()

        # Backpropagate error
        loss.backward()

        # Optimize
        nn.utils.clip_grad_norm_(model.parameters(), args.max_grad_norm)
        optimizer.step()

        # Log
        if (batch_idx + 1) % (args.report_every) == 0:
            print(
                'Epoch {0} ; Iteration {1}/{2} ; Learning rate {3} ; Perplexity {4}'.format(epoch_n + 1, batch_idx + 1,
                                                                                            n_iterations, lr, np.exp(
                        total_loss / tgt_word_count)))
            total_loss = 0
            src_word_count = 0
            tgt_word_count = 0

    return total_loss / tgt_word_count


def train(model, dataset_train, dataset_valid, embeddings_enc, embeddings_dec):
    # Optimizer
    for param in model.parameters():
        param.data.uniform_(-args.param_init, args.param_init)
    optimizer = optim.SGD(model.parameters(), lr=args.learning_rate)

    for i in range(args.epochs):
        # Train epoch
        trainEpoch(model, dataset_train, optimizer, i, embeddings_enc, embeddings_dec)

        # Validate epoch
        perp = validateEpoch(model, dataset_valid, embeddings_enc, embeddings_dec)

        # Update learning date
        updateLearningRate(optimizer, i)

        # Save model
        saveModel(model, '{0}.{1}.{2}.pth'.format(args.save_model, i + 1, round(perp, 4)))


def main():
    # Load and prepare data
    print('Loading data from \'' + args.data + '\'...')
    data = torch.load(args.data)

    train_data = data['train']
    valid_data = data['valid']

    src_dict_size = data['dicts']['src'].size()
    tgt_dict_size = data['dicts']['tgt'].size()

    # Create dataset
    dataset_train = DatasetTrain(train_data)
    dataset_valid = DatasetTrain(valid_data)

    # Pre-trained embeddings
    embeddings_encoder = to_device(torch.load(args.pre_word_vecs_enc))
    embeddings_enc = nn.Embedding.from_pretrained(embeddings_encoder,
                                                  freeze=True)  # If freeze False the tensor gets updated in the learning process

    embeddings_decoder = to_device(torch.load(args.pre_word_vecs_dec))
    embeddings_dec = nn.Embedding.from_pretrained(embeddings_decoder,
                                                  freeze=True)  # If freeze False the tensor gets updated in the learning process

    # Build model
    if args.rnn_size % 2 != 0:
        print('rnn size must be divisible by 2')
    encoder = to_device(Encoder(src_dict_size, args.word_vec_size, int(args.rnn_size / 2), args.layers, args.dropout))
    decoder = to_device(
        Decoder(tgt_dict_size, args.word_vec_size, args.rnn_size, args.layers, args.dropout, args.attn_mode))

    weight = to_device(torch.ones(tgt_dict_size))
    weight[constants.PAD] = 0
    criterion = nn.NLLLoss(weight=weight, size_average=False)  # TODO: reduce

    model = Generator(encoder, decoder, criterion, data['dicts']['src'], data['dicts']['tgt'])

    # Count parameters
    n_params = sum(p.numel() for p in model.parameters())
    n_params_train = sum(p.numel() for p in model.parameters() if p.requires_grad)

    # Print
    print(' * vocabulary size: source = {0}; target = {1}'.format(src_dict_size, tgt_dict_size))
    print(' * number of training sentences: {0}'.format(len(train_data['src'])))
    print(' * maximum batch size: {0}'.format(args.max_batch_size))
    print(' * number of parameters: {0}'.format(n_params))
    print(' * number of trainable parameters: {0}'.format(n_params_train))

    # Train
    train(model, dataset_train, dataset_valid, embeddings_enc, embeddings_dec)


if __name__ == "__main__":
    main()
