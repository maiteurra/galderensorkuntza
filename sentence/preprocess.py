import torch
import argparse

from utils import Dict, constants

parser = argparse.ArgumentParser(description='preprocess')

# Data
parser.add_argument('--train_src', type=str, default='', required = True, help='path to the training source data')
parser.add_argument('--train_tgt', type=str, default='', required = True, help='path to the training target data')
parser.add_argument('--valid_src', type=str, default='', required = True, help='path to the validation source data')
parser.add_argument('--valid_tgt', type=str, default='', required = True, help='path to the validation target data')

parser.add_argument('--save_data', type=str, default='', required = True, help='output file for the prepared data')

# Vocabulary
parser.add_argument('--src_vocab_size', type=int, default=50000, help='size of the source vocabulary')
parser.add_argument('--tgt_vocab_size', type=int, default=50000, help='size of the target vocabulary')

parser.add_argument('--src_vocab', type=str, default='', help='path to an existing source vocabulary')
parser.add_argument('--tgt_vocab', type=str, default='', help='path to an existing target vocabulary')

# Other
parser.add_argument('--src_seq_length', type=int, default=50, help='maximum source sequence length')
parser.add_argument('--tgt_seq_length', type=int, default=50, help='maximum target sequence length')

parser.add_argument('--report_every', type=int, default=100000, help='report status every this many sentences')


# Parse arguments
args = parser.parse_args()


def makeVocabulary(filename, size):
	wordVocab = Dict([constants.PAD_WORD, constants.UNK_WORD, constants.BOS_WORD, constants.EOS_WORD])
	with open(filename, 'r', encoding='utf-8') as f:
		for line in f:
			words = line.split()
			for word in words:
				wordVocab.add(word)

	originalSize = wordVocab.size()
	wordVocab = wordVocab.prune(size)
	print('Created dictionary of size ' + str(wordVocab.size()) + ' (pruned from ' + str(originalSize) + ')')

	return wordVocab


def initVocabulary(name, dataFile, vocabFile, vocabSize):
	wordVocab = None

	if len(vocabFile) > 0:
		print('Reading '+ name + 'vocabulary from \'' + vocabFile + '\'...')
		wordVocab = Dict()
		wordVocab.loadFile(vocabFile)
		print('Loaded '+ str(wordVocab.size()) + ' ' + name + ' words')

	if wordVocab == None:
		print('Building ' + name + ' vocabulary...')
		wordVocab = makeVocabulary(dataFile, vocabSize)

	return wordVocab

def saveVocabulary(name, vocab, file):
	print('Saving ' + name + ' vocabulary to \'' + file + '\'...')
	vocab.writeFile(file)


def makeData(srcFile, tgtFile, srcDicts, tgtDicts):

	src = []
	tgt = []

	sizes = []
	
	sf = open(srcFile, 'r', encoding='utf-8')
	tf = open(tgtFile, 'r', encoding='utf-8')

	srcWords = [line.split() for line in sf.read().splitlines()]
	tgtWords = [line.split() for line in tf.read().splitlines()]

	if(len(srcWords) != len(tgtWords)):
		print('WARNING: source, target do not have the same number of sentences')
		return

	ignored = 0

	for i in range(len(srcWords)):

		if len(srcWords[i]) > 0 and len(srcWords[i]) < args.src_seq_length and len(tgtWords[i]) > 0 and len(tgtWords[i]) < args.tgt_seq_length:

			src.append(srcDicts.convertToIdx(srcWords[i], unkWord=constants.UNK_WORD))
			tgt.append(tgtDicts.convertToIdx(tgtWords[i], unkWord=constants.UNK_WORD, bosWord=constants.BOS_WORD, eosWord=constants.EOS_WORD))

			sizes.append(len(srcWords[i]))

		else:
			ignored += 1
 
		if (i+1) % args.report_every == 0:
			print('... ' + str((i+1)) + ' sentences prepared')

	
	def reorder(tab, index):
			newTab = []

			for i in range(len(tab)):
				newTab.append(tab[index[i]])

			return newTab


	def reorderData(perm, src, tgt):
	
		src = reorder(src,perm)
		tgt = reorder(tgt,perm)

		return src, tgt


	print('Sorting sentences by size')
	_, perm = torch.sort(torch.Tensor(sizes), descending=False)
	src, tgt = reorderData(perm, src, tgt)

	print('Prepared ' + str(len(src)) + ' sentences (' + str(ignored) + ' ignored due to source length > ' + str(args.src_seq_length) + ' or target length > ' + str(args.tgt_seq_length) + ')' )
	
	return src, tgt



def main():

	data = {}

	# Init vocabularies
	data['dicts'] = {}
	data['dicts']['src'] = initVocabulary('source', args.train_src, args.src_vocab, args.src_vocab_size)
	data['dicts']['tgt'] = initVocabulary('target', args.train_tgt, args.tgt_vocab, args.tgt_vocab_size)


	# Prepare training data
	print('')
	print('Preparing training data...')
	data['train'] = {}
	data['train']['src'], data['train']['tgt'] = makeData(args.train_src, args.train_tgt, data['dicts']['src'], data['dicts']['tgt'])

	
	# Prepare validation data
	print('')
	print('Preparing validation data...')
	data['valid'] = {}
	data['valid']['src'], data['valid']['tgt'] = makeData(args.valid_src, args.valid_tgt, data['dicts']['src'], data['dicts']['tgt'])
	print('')

	# Save vocabularies
	if len(args.src_vocab) == 0:
		saveVocabulary('source', data['dicts']['src'], args.save_data + '.src.dict')

	if len(args.tgt_vocab) == 0:
		saveVocabulary('target', data['dicts']['tgt'], args.save_data + '.tgt.dict')


	# Save data
	print('Saving data to \'' + args.save_data + '-train.pth\'...')
	torch.save(data, args.save_data + '-train.pth')



if __name__ == "__main__":
	main()