import argparse

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from torch.utils.data.sampler import SequentialSampler
from .utils import DatasetGenerate, Padder, device, constants

to_device = device.gpu if torch.cuda.is_available else device.cpu

parser = argparse.ArgumentParser(description='generate')

parser.add_argument('--src', type=str, default='', required=True,
                    help='Source sequence to decode (one line per sequence)')
parser.add_argument('--output', type=str, default='pred.txt',
                    help='Path to output the predictions (each line will be the decoded sequence')
parser.add_argument('--model', type=str, default='', required=True, help='Path to model .pth file')
parser.add_argument('--beam_size', type=int, default=5, required=True, help='Beam size')
parser.add_argument('--batch_size', type=int, default=1, help='Batch size')
parser.add_argument('--max_sent_length', type=int, default=250,
                    help='Maximum sentence length. If any sequences in srcfile are longer than this then it will error out')
parser.add_argument('--replace_unk', type=bool, default=False, required=True,
                    help='Replace the generated UNK tokens with the source token that had the highest attention weight. If phrase_table is provided, it will lookup the identified source token and give the corresponding target token. If it is not provided (or the identified source token does not exist in the table) then it will copy the source token')
parser.add_argument('--phrase_table', type=str, default='',
                    help='Path to source-target dictionary to replace UNK tokens. See README.md for the format this file should be in')
parser.add_argument('--pre_word_vecs_enc', type=str, default='',
                    help='If a valid path is specified, then this will load pretrained word embeddings on the encoder side. See README for specific formatting instructions.')
parser.add_argument('--pre_word_vecs_dec', type=str, default='',
                    help='If a valid path is specified, then this will load pretrained word embeddings on the decoder side. See README for specific formatting instructions.')

# Parse arguments
args = parser.parse_args()


def replace_unk(seq_words, src_words, sequence_attns):
    for i, word in enumerate(seq_words):
        if word == constants.UNK_WORD:
            idx = torch.argmax(sequence_attns[i])
            seq_words[i] = src_words[idx]

    return seq_words


def main():
    # Open file
    f = open(args.output, 'w', encoding='utf-8', errors='surrogateescape')

    # Load model
    print('Loading {0}'.format(args.model))
    generator = torch.load(args.model)

    # Load source
    dataset = DatasetGenerate(args.src, generator.src_dictionary)
    loader = DataLoader(dataset, args.batch_size, sampler=SequentialSampler(dataset), drop_last=False,
                        collate_fn=Padder(dim=0, tgt=False))
    sf = open(args.src, 'r', encoding='utf-8')
    src_words = [line.split() for line in sf.read().splitlines()]
    print(len(src_words))

    # Pre-trained embeddings
    embeddings_encoder = to_device(torch.load(args.pre_word_vecs_enc))
    embeddings_enc = nn.Embedding.from_pretrained(embeddings_encoder,
                                                  freeze=True)  # If freeze False the tensor gets updated in the learning process

    embeddings_decoder = to_device(torch.load(args.pre_word_vecs_dec))
    embeddings_dec = nn.Embedding.from_pretrained(embeddings_decoder,
                                                  freeze=True)  # If freeze False the tensor gets updated in the learning process

    # Generate questions
    for idx, (src, lengths) in enumerate(loader):
        src_words_seq = src_words[idx]
        print('SENT {0}: '.format(idx + 1), end='')
        print(*src_words_seq)

        src = to_device(src)
        sequences, sequence_scores, sequence_attns = generator.beam_search(src, lengths, embeddings_enc, embeddings_dec,
                                                                           args.beam_size, args.max_sent_length)

        question_words = generator.tgt_dictionary.convertToWords(sequences[0][:-1])
        question_words = replace_unk(question_words, src_words_seq, sequence_attns[0][:])

        print('PRED {0}: '.format(idx + 1), end='')
        print(*question_words)
        print('PRED SCORE: {0}'.format(sequence_scores[0]))
        print('')

        print(*question_words, file=f)


if __name__ == "__main__":
    main()
