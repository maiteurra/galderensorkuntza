# Galderen sorkuntza ikasketa sakoneko tekniken bidez

Kodea exekutatzeko jarraitu beharreko pausoak:
```
cd sentence/scripts
```

## Aurreprozesaketa

#### Hiztegiak eta datuak lortu

```
./preprocess.sh
```

#### Esanahi-bektoreak lortu

```
./preprocess_embedding.sh
```

```
./convert.sh
```

## Entrenatu

```
./train.sh
```

## Sortu

```
./generate.sh
```

## Ebaluatu

```
./evaluate.sh
```